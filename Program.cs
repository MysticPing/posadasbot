﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using Discord.Commands;
using System.Data.SQLite;

namespace PosadasBot
{
    class Program
        {
        private DiscordSocketClient _client;
        private SQLiteConnection _con;
        public static void Main(string[] args)
            => new Program().MainAsync().GetAwaiter().GetResult();

        public async Task MainAsync()
        {
            _client = new DiscordSocketClient();
            _client.Log += Log;
            _client.ReactionAdded += ReactionAddedAsync;
            _client.JoinedGuild += JoinedGuildASync;

            // tests are permament. Load if exists, otherwise create.
            _con = new SQLiteConnection("URI=file:"+Environment.CurrentDirectory+@"\test.db");
            _con.Open();
            using var cmd = new SQLiteCommand(_con);
            cmd.CommandText = "CREATE TABLE IF NOT EXISTS guilds (guild INTEGER PRIMARY KEY, channel INTEGER, emoji TEXT, count INTEGER);";
            cmd.ExecuteNonQuery();

            using (var services = ConfigureServices())
            {
                var client = services.GetRequiredService<DiscordSocketClient>();

                client.Log += Log;
                services.GetRequiredService<CommandService>().Log += Log;

                // Tokens should be considered secret data and never hard-coded.
                // We can read from the environment variable to avoid hardcoding.
                await client.LoginAsync(TokenType.Bot, Environment.GetEnvironmentVariable("DiscordToken"));
                await client.StartAsync();

                // Here we initialize the logic required to register our commands.
                await services.GetRequiredService<CommandHandlingService>().InstallCommandsAsync();
                
                await Task.Delay(-1);
            }
        }

        private async Task JoinedGuildASync(SocketGuild guild)
        {
            // Inserts guild into SQLite db with empty values.
            // TODO: Send a hello message to default channel with instructions.
            using var cmd = new SQLiteCommand(_con);
            cmd.CommandText = "INSERT INTO guilds (guild, channel, emoji, count) VALUES(@guildId, @channelId, @emoji, @count)";
            cmd.Parameters.AddWithValue("@guildId", guild.Id);
            cmd.Parameters.AddWithValue("@channelId", 0);
            cmd.Parameters.AddWithValue("@emoji", "");
            cmd.Parameters.AddWithValue("@count", 0);
            await cmd.ExecuteNonQueryAsync();
        }

        private async Task ReactionAddedAsync(Cacheable<IUserMessage, ulong> message, ISocketMessageChannel channel, SocketReaction reaction)
        {
            await Log(new Discord.LogMessage(0, "React", "Added"));

            // Find guild settings.
            ulong guildId = (channel as SocketTextChannel).Guild.Id;
            using var cmd = new SQLiteCommand(_con);
            cmd.CommandText = "SELECT * FROM guilds WHERE guild=@guildId";
            cmd.Parameters.AddWithValue("@guildId",guildId);
            SQLiteDataReader rdr = cmd.ExecuteReader();
            await rdr.ReadAsync();
            ulong  reactGuild   = (ulong) rdr.GetInt64(0);
            string reactEmoji   = rdr.GetString(2);
            uint   reactCount   = (uint) rdr.GetInt32(3);
            ulong  reactChannel = (ulong) rdr.GetInt64(1);
            await rdr.CloseAsync();
            
            // Check if correct amount of correct emoji
            Emoji star = new Emoji(reactEmoji);
            IUserMessage reactedMessage = await message.GetOrDownloadAsync();
            ICollection<IUser> reactions = await reactedMessage.GetReactionUsersAsync(star, 100).FlattenAsync() as ICollection<IUser>;
            if (reactions.Count == reactCount) {
                // Build and send rich embed
                EmbedBuilder e = new EmbedBuilder();
                e.Author = new EmbedAuthorBuilder();
                e.Author.Name = reactedMessage.Author.Username;
                e.Author.IconUrl = reactedMessage.Author.GetAvatarUrl();

                e.Color = Color.Red;

                e.Title = "";
                e.Description = reactedMessage.Content;
                if (reactedMessage.Attachments.Count > 0)
                    e.ImageUrl = reactedMessage.Attachments.First().Url;
                e.AddField("Author", reactedMessage.Author.Mention, true);
                e.AddField("Channel", MentionUtils.MentionChannel(reactChannel), true);

                SocketTextChannel stc = _client.GetChannel(reactChannel) as SocketTextChannel;
                await stc.SendMessageAsync("", false, e.Build());
                await Log(new Discord.LogMessage(0, "React", "Pinned"));
            }
        }

        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }

        private ServiceProvider ConfigureServices()
        {
            // Pass down the SQLiteConnection
            return new ServiceCollection()
                .AddSingleton<DiscordSocketClient>(_client)
                .AddSingleton<CommandService>()
                .AddSingleton<CommandHandlingService>()
                .AddSingleton<SQLiteConnection>(_con)
                .BuildServiceProvider();
        }
    }
}
