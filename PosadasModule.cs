using System;
using System.Data.SQLite;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;

namespace PosadasBot
{
    public class Posadas : ModuleBase<SocketCommandContext>
    {
        private readonly SQLiteConnection _con;
        public Posadas (SQLiteConnection con) 
        {
            _con = con;
        }

        
        [Command("help")]
        public async Task SquareAsync()
        {
            await Context.Channel.SendMessageAsync("```!emote [emoji] sets the emote.\n!count [integer] sets the amount of reacts before pin.\n!channel [#channel] sets the channel.```");
        }

        [Command("channel")]
        public async Task ChannelAsync(SocketTextChannel stc)
        {
            // Find guild settings.
            ulong guildId = Context.Guild.Id;
            using var cmd = new SQLiteCommand(_con);
            cmd.CommandText = "SELECT * FROM guilds WHERE guild=@guildId";
            cmd.Parameters.AddWithValue("@guildId",guildId);
            SQLiteDataReader rdr = cmd.ExecuteReader();
            await rdr.ReadAsync();
            ulong  reactGuild   = (ulong) rdr.GetInt64(0);
            string reactEmoji   = rdr.GetString(2);
            uint   reactCount   = (uint) rdr.GetInt32(3);
            ulong  reactChannel = stc.Id;
            await rdr.CloseAsync();

            // Replace with new channel.
            cmd.CommandText = "REPLACE INTO guilds (guild, channel, emoji, count) VALUES (@guild, @channel, @emoji, @count)";
            cmd.Parameters.AddWithValue("@guild",   reactGuild);
            cmd.Parameters.AddWithValue("@channel", reactChannel);
            cmd.Parameters.AddWithValue("@emoji",   reactEmoji);
            cmd.Parameters.AddWithValue("@count",   reactCount);
            await cmd.ExecuteNonQueryAsync();

            await ReplyAsync("Pin channel set.");
        }

        
        [Command("emote")]
        public async Task EmoteAsync(string emote)
        {
            // Find guild settings.
            ulong guildId = Context.Guild.Id;
            using var cmd = new SQLiteCommand(_con);
            cmd.CommandText = "SELECT * FROM guilds WHERE guild=@guildId";
            cmd.Parameters.AddWithValue("@guildId",guildId);
            SQLiteDataReader rdr = cmd.ExecuteReader();
            await rdr.ReadAsync();
            ulong  reactGuild   = (ulong) rdr.GetInt64(0);
            string reactEmoji   = emote;
            uint   reactCount   = (uint) rdr.GetInt32(3);
            ulong  reactChannel = (ulong) rdr.GetInt64(1);
            await rdr.CloseAsync();

            // Replace with new channel.
            cmd.CommandText = "REPLACE INTO guilds (guild, channel, emoji, count) VALUES (@guild, @channel, @emoji, @count)";
            cmd.Parameters.AddWithValue("@guild",   reactGuild);
            cmd.Parameters.AddWithValue("@channel", reactChannel);
            cmd.Parameters.AddWithValue("@emoji",   reactEmoji);
            cmd.Parameters.AddWithValue("@count",   reactCount);
            await cmd.ExecuteNonQueryAsync();

            await ReplyAsync("React emoji set.");
        }

        [Command("count")]
        public async Task EmoteAsync(uint count)
        {
            // Find guild settings.
            ulong guildId = Context.Guild.Id;
            using var cmd = new SQLiteCommand(_con);
            cmd.CommandText = "SELECT * FROM guilds WHERE guild=@guildId";
            cmd.Parameters.AddWithValue("@guildId",guildId);
            SQLiteDataReader rdr = cmd.ExecuteReader();
            await rdr.ReadAsync();
            ulong  reactGuild   = (ulong) rdr.GetInt64(0);
            string reactEmoji   = rdr.GetString(2);
            uint   reactCount   = count;
            ulong  reactChannel = (ulong) rdr.GetInt64(1);
            await rdr.CloseAsync();

            // Replace with new channel.
            cmd.CommandText = "REPLACE INTO guilds (guild, channel, emoji, count) VALUES (@guild, @channel, @emoji, @count)";
            cmd.Parameters.AddWithValue("@guild",   reactGuild);
            cmd.Parameters.AddWithValue("@channel", reactChannel);
            cmd.Parameters.AddWithValue("@emoji",   reactEmoji);
            cmd.Parameters.AddWithValue("@count",   reactCount);
            await cmd.ExecuteNonQueryAsync();

            await ReplyAsync("React count set.");
        }
    }
}